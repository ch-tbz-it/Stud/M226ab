## Wissensaneignung UML 

Zeitbedarf ca. 40-60 min
das Thema ist prüfungsrelevant

- Formulieren Sie mit eigenen Worten und geben Sie hier ein PDF ab.
- Was heisst UML und wofür ist was (welche Darstellung) gut?
- Reichen diese Darstellungen um ein System zu beschreiben?  Gibt es weitere Möglichkeiten?

**gehen Sie so vor**:
 - Schauen Sie sich diese Videos an. 
 - Schauen Sie parallel dazu im Internet, zB bei Wikipedia, ergänzend auch noch die entsprechenden Beschreibungen an. 
 - Schauen Sie nach weiteren Darstellungen im Internet.
 - Schauen Sie im Schulzimmer die aufgehängten UML-Darstellung(en) an.

Videos zu UML
- (13:23 min) UML **Use Case Diagram**, Anwendungsfall-Diagramm Tutorial
<https://www.youtube.com/watch?v=zid-MVo7M-E> 

- (10:16 min) UML **Class Diagram**, Klassendiagramm Tutorial
<https://www.youtube.com/watch?v=UI6lqHOVHic> 

- (8:37 min) UML **Sequence Diagram**, Sequenzdiagramm Tutorial
<https://www.youtube.com/watch?v=pCK6prSq8aw>  

- (7:31 min) **Klassendiagramme** mit UML
Theoretische Objektorientierte Konzepte 1
<https://www.youtube.com/watch?v=zzwUH3vbNkc> 


Bewertung: 
keine --> Abgabe heisst: "Videos gesehen"