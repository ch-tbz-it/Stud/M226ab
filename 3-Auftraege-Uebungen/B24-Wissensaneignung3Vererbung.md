## Wissensaneignung 3 (Vererbung)

- [Buch Java9 Kap.8, E. Fuchs, 2017, HERDT](../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/JAV9.pdf)

Zeitbedarf 1-2 Std.

Lesen Sie Kapitel 8 (S. 89-109) aufmerksam durch, lösen Sie dann die Übung (S. 109) und geben Sie sie ab.

<../2-Unterlagen/00-Buecher/Buch__Java_9_Grundlagen_Programmierung/>