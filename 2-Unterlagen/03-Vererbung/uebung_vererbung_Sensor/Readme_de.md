Sensor übung
============

Dieser Code ist eine Lösung zur übung.


Übung zu Vererbung, abstrakten Klassen und Polymorphismus
-----------------
Mit dieser Übung sollen an einem praktischen Beispiel die Vererbung, der Einsatz von abstrakten Klassen zur Spezifizierung von Klassen, gezeigt werden. Bei der Benutzung der konkreten Implementationen der Abstrakten Klasse soll dann Polymorphismus angewandt werden.

Erstellen sie eine Klassenhierarchie, die mehrere verschiedene Sensoren enthält. Einige Instanzen dieser Sensoren sollten sie dann ansprechen und die Messwerte in verschiedenen CSV-Files speichern. Eine Abstrakte Klasse soll die Spezifikation von allen Sensoren enthalten, und konkrete Implementationsklassen sollen dann die Details implementieren. Dabei wird eine 2-Stufige Spezifikationshierarchie aufgebaut.

Aufgaben
-

1) **_Spezifikation:_**

    Erzeugen sie ein Abstrakte Klasse Sensor mit den folgenden abstrakten Methoden:

       getUnit(): Diese soll die Einheit der Messung zurückgeben wie zum Beispiel Grad Celsius

       getValue(): Die Messung in der Einheit, d.h. ein double wie zum Beispiel 17.0

       getName(): Diese soll nur den Namen des Messwerts zurückgeben, wie zum Beispiel "Aussentemperatur in Baar"

       doMeasurement(): Diese soll die Messung wirklich durchführen, d.h. einen "Sensor auslesen" zum Beispiel indem eine serielle Schnittstelle angesprochen wird oder ein HTTP-Request gemacht wird oder etwas anderes.


2) **_Spezifikation/Teil-Implementation:_**
   
   Erzeugen sie eine abstrakte Klasse für alle Drucksensoren mit dem Namen PressureSensor. Diese soll Sensor erweitern.

   Diese soll folgende Methoden verwirklichen und folgende Felder definieren:

        private String unit;

        protected double measurementValue;

        getValue(): gibt einfach die Variable measurementValue zurück.

        getUnit(): gibt einfach die Variable unit zurück.


3) **_Implementation:_**

   Erzeugen sie eine Kind-Klasse von PressureSensor Barometric1000PressureSensorImpl die folgende Methode implementiert:

        doMeasurement(): Diese solle einen Wert zwischen 0.5 und 1.05 in die measurmentValue Variable schreiben. 

   Hier würde in der Realität der physische Sensor über eine Schnittstelle des Sensors angesprochen. Das wäre dann sehr herstellerspezifisch und könnte von Modell zu Modell des Herstellers auch noch ändern. Das heisst, sie müssten die Manuals des Herstellers von Barometric1000 lesen, um diese Methode in der Realität zu programmieren. Aber wir machen es uns einfach und schreiben einfach einen Zufallswert in die Variable measurmentValue.


4) **_Implementation:_**

   Erzeugen sie eine Kind-Klasse von PressureSensor Aqualung2021ProDivePressureSensorImpl und implementieren sie die Methode doMeasurment() die Werte zwischen 0 und 10.0  zurückgibt. Auch hier müssten sie in der Realität die Manuals von Aqualung2021Pro lesen.


5) **_Spezifikation/Teil-Implementation:_**

   Erzeugen und Teil-Implementieren sie eine der folgenden Abstrakten Kind-Klassen von Sensor analog zur Klasse PressureSensor:

         abstract class TemperatureSensor

         abstract class HumiditySensor

         abstract class SpeedSensor



6) **_Implementation:_**

   Implementieren sie 2 Konkrete Klassen für zwei Modelle der von ihnen in Aufgabe 5 spezifizierten Abstrakten Klasse.


7) **_Benutzen von Polymorphismus:_**

   **--> ** **_Schreiben sie jetzt eine Klasse CsvWriter_** welche von einem dieser Sensoren in einem vorgegeben Zeitintervall Messwerte ausliest und diese dann in ein File schreibt. (Filenamen, Intervall und Sensor soll konfigurierbar sein).
   - Tipp: Sie erzeugen ein Objekt einer Kind-Klasse von Sensor und übergeben diese an dem Konstruktor von CsvWriter als Parameter zusammen mit dem Intervall und dem Filenamen.

     Fileformat: jede Zeile ist Comma Separated Value (CSV) d.h. sowas wie _time stamp_,_name of sensor_,_unit_,_measurement_. Jede Zeile entspricht einer Messung.
   - Implementieren sie eine Methode "public void run()" welche sie dann aufrufen um die Messungen wirklich zu machen und diese ins File zu schreiben.


8) **--> ** Erzeugen sie den CsvWriter in einer Main-Klasse und ermöglichen sie dem Benutzer den Sensor, das Intervall und den Filenamen auszuwählen.


9) **--> ** **_Optional:_** Überlegen sie sich, wie sie dieselben Daten in eine Datenbank schreiben würden.


