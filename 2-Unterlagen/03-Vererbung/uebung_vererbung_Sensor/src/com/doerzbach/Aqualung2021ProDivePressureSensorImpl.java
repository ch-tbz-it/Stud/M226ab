package com.doerzbach;

public class Aqualung2021ProDivePressureSensorImpl extends PressureSensor{
    @Override
    public String getName() {
        return "Aqualung2021ProDive";
    }

    @Override
    public void doMeasurement() {
        measurementValue=10.0*Math.random();

    }
}
