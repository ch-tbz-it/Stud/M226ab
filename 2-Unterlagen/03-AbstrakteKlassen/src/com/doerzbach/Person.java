package com.doerzbach;



public abstract class Person {
    private String username;
    private String passwort;
    private String nachName;
    private String vorName;
    private String emailAdresse;
    private String telefonNummer;
    private String mobileNummer;
    private Geschlecht geschlecht;

    public boolean checkPasswd(String passwort){

        return this.passwort.equals(passwort);
    };

    public void setGeschlecht(Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public String getUsername() {
        return username;
    }

    public Person(String username, String passwort, String nachName, String vorName, String emailAdresse, String telefonNummer, String mobileNummer, Geschlecht geschlecht) {
        this.username = username;
        this.passwort = passwort;
        this.nachName = nachName;
        this.vorName = vorName;
        this.emailAdresse = emailAdresse;
        this.telefonNummer = telefonNummer;
        this.mobileNummer = mobileNummer;
        this.geschlecht = geschlecht;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public void setNachName(String nachName) {
        this.nachName = nachName;
    }

    public void setVorName(String vorName) {
        this.vorName = vorName;
    }

    public void setEmailAdresse(String emailAdresse) {
        this.emailAdresse = emailAdresse;
    }

    public void setTelefonNummer(String telefonNummer) {
        this.telefonNummer = telefonNummer;
    }

    public void setMobileNummer(String mobileNummer) {
        this.mobileNummer = mobileNummer;
    }

    public String getNachName() {
        return nachName;
    }

    public String getVorName() {
        return vorName;
    }

    public String getEmailAdresse() {
        return emailAdresse;
    }

    public String getTelefonNummer() {
        return telefonNummer;
    }

    public String getMobileNummer() {
        return mobileNummer;
    }
    public abstract String getAnrede();

    public abstract String getPasswordPrompt();
}
