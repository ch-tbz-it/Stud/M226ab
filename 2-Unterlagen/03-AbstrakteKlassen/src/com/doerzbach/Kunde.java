package com.doerzbach;

public class Kunde extends Person {
    private int kundenNummer;
    private float umsatz;

    public Kunde(int kundenNummer,String username, String passwort, String nachName, String vorName, String emailAdresse, String telefonNummer, String mobileNummer, Geschlecht geschlecht,float umsatz) {
        super(username, passwort, nachName, vorName, emailAdresse, telefonNummer, mobileNummer, geschlecht);
        this.kundenNummer=kundenNummer;
        this.umsatz = umsatz;
    }

    public int getKundenNummer() {
        return kundenNummer;
    }


    @Override
    public String getAnrede() {
        return "Sehr geehrter "+getGeschlecht()+" "+getNachName();
    }

    @Override
    public String getPasswordPrompt() {
        return "Geben sie ihr Passwort ein:";
    }
}
